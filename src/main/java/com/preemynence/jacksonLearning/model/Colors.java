package com.preemynence.jacksonLearning.model;

import lombok.Data;

import java.util.List;

@Data
public class Colors {
	List<Color> colors;
}