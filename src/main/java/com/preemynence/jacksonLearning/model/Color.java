package com.preemynence.jacksonLearning.model;

import lombok.Data;

@Data
public class Color {
	private String color;
	private String category;
	private String type;
	private Code code;
}
