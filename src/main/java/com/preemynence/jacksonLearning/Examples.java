package com.preemynence.jacksonLearning;

public interface Examples {

	String FILE_NAME = "target/streamExample.json";

	void runExample();
}
